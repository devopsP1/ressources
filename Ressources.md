# Ressources externes pour l'apprentissage dde Linux et de la programmation

## MOOC

### Définition

MOOC (formation en ligne massive ouverte à tous)
Qu’est ce qu’un MOOC ? – Définition par l’agence 1min30
Le signe anglophone MOOC désigne les “Massive Open Online Courses“. En français, on pourrait traduire le terme par “formation en ligne massive ouverte à tous“, ce qui donnerait FLMOT sous forme de sigle. Pas terrible.

#### Liste de MOOC par sujets

##### Linux / bash

- [Apprendre Linux sur FUN MOOC. ](https://fun-mooc.fr/apprendre-linux.html)  :  très bien pour les débutants

- [L'essentiel pour maîtriser Linux. ](https://www.fun-mooc.fr/courses/ucarthage/115001/session01/about)  :  s'appronfondir dans linux

- [Maîtriser le shell Bash - session 2. ](https://www.fun-mooc.fr/courses/course-v1:univ-reunion+128001+session02/about)  :  enseignement sur la maitrise du bash

- suite de la liste 


##### Python
- [Apprendre le language python. ](https://www.dealabs.com/bons-plans/cours-pour-apprendre-a-coder-en-python-gratuit-dematerialise-francais-fun-moocfr-1491889)  :  des formatiions en ligne

- [Apprendre le langage de programmation python. ](https://python.doctor/)  :  le site pour apprandre le language python pour les débutants

- [Python : des fondamentaux à l'utilisation du langage. ](https://www.fun-mooc.fr/courses/inria/41001S03/session03/about)  :  formation permettant a se renforcer au python

##### Mooc généraux sur l'informatique

-[Fabriquer un objet connecté - Session1. ](https://www.fun-mooc.fr/courses/MinesTelecom/04018/session01/about)


## Sites web
-[Apprenez à créer votre site web avec HTML5/ CSS3. ](https://mooc-francophone.com/cours/mooc-apprenez-a-creer-votre-site-web-avec-html5-css3/)  :  Je conseille ce site aux débutants

-[Supervision de Réseaux et Services. ](https://www.fun-mooc.fr/courses/course-v1:lorraine+30008+session03/about)  :  Je le conseillerai pour ceux qui en n'ont déja gouter de l'informatique

-[Internet of Things with Microcontrollers: a hands-on course / L'Internet des Objets sur microcontrôleurs par la pratique. ](https://www.fun-mooc.fr/courses/course-v1:inria+41020+session01/about)  :  ici ce serai pour les passionnées d'appareille de connection a distance avec un ordinateur


## Applications mobiles
-[Développez votre première application Android. ](https://mooc-francophone.com/cours/developpez-votre-premiere-application-android/)

- [UN NOUVEAU MOOC POUR APPRENDRE À CRÉER UNE APPLICATION DE RÉALITÉ AUGMENTÉE. ](https://www.mondedesgrandesecoles.fr/nouveau-mooc/)




## Applications desktop
-[Créez votre première application connectée en C# / .NET; ](https://mooc-francophone.com/cours/mooc-creez-votre-premiere-application-connectee-en-c-net/)

-[Développez vos compétences grâce à des cours. ](https://fr.coursera.org/)  :  Atteignez vos objectifs avec Coursera

## Livres & magazines
-  [Mooc une forme contemporaine de livres éducatifs. ](https://journals.openedition.org/dms/1830)

-  [Les Moocs pour ou contre](https://compassmag.3ds.com/fr/3/Education/LES-MOOCs-POUR-OU-CONTRE)

-  [SIRH : un Mooc gratuit pour le mettre en place. ](https://www.solutions-numeriques.com/sirh-un-mooc-gratuit-pour-le-mettre-en-place/)

-  [Marketers, quel Mooc choisir pour être au top?. ](https://www.e-marketing.fr/Thematique/management-1090/Diaporamas/quel-mooc-choisir-top-330150/gerez-votre-temps-heure-digital-330155.htm#Diapo)

